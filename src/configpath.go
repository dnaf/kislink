package main

import (
	"errors"
	"log"
	"os"
	"path"
)

var NoConfigFound = errors.New("No config file was found")

func testDirectoryForConfig(dir string) (string, error) {
	{
		dirinfo, err := os.Stat(dir)
		if err != nil {
			if os.IsNotExist(err) {
				return "", NoConfigFound
			}
			return "", err
		}

		if !dirinfo.IsDir() {
			return "", NoConfigFound
		}
	}

	for _, ext := range [...]string{"yml", "yaml"} {
		p := path.Join(dir, "config."+ext)
		log.Printf("testing %q\n", p)

		info, err := os.Stat(p)
		if err != nil {
			if os.IsNotExist(err) {
				continue
			} else {
				return "", err
			}
		}

		if info.IsDir() {
			continue
		}

		return p, nil
	}

	return "", NoConfigFound
}

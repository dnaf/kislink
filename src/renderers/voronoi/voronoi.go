/*

   kislink
   Copyright (C) 2018  Katie Wolfe

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package voronoi

import (
	"github.com/lucasb-eyer/go-colorful"
	"image"
	"log"
	"math"
	"math/rand"
)

type point struct {
	X, Y float64
}
type mpoint struct {
	point
	Vx, Vy float64
}

func (p1 *point) Dist(p2 point) float64 {
	a := (p2.X - p1.X)
	b := (p2.Y - p1.Y)
	return math.Sqrt(a*a + b*b)
}

type Voronoi struct {
	*image.RGBA

	fps float64

	width, height int

	points []mpoint
	colors []colorful.Color
}
type VoronoiOpts struct {
	Width, Height int
	NumPoints     int
}

func (self *Voronoi) Init(l *log.Logger, optsi interface{}) {
	opts := optsi.(*VoronoiOpts)
	self.width = opts.Width
	self.height = opts.Height
	self.RGBA = image.NewRGBA(image.Rect(0, 0, self.width, self.height))
	self.fps = 60

	r := rand.New(rand.NewSource(298757))

	numpoints := opts.NumPoints
	self.points = make([]mpoint, numpoints)
	self.colors = make([]colorful.Color, numpoints)
	for i := range self.points {
		self.points[i] = mpoint{
			point: point{
				r.Float64() * float64(self.width),
				r.Float64() * float64(self.height),
			},
			Vx: (r.Float64() - 0.5) * 2.0,
			Vy: (r.Float64() - 0.5) * 2.0,
		}
		self.colors[i] = colorful.Hsv(float64(i)/float64(numpoints)*140.0+200.0, 1.0, r.Float64()*0.2)
	}
}

func (self *Voronoi) GetDefaultOptions() interface{} {
	return &VoronoiOpts{
		Width:     16,
		Height:    16,
		NumPoints: 5,
	}
}

func (self *Voronoi) Buffer(l *log.Logger) {
	for x := 0; x < self.width; x++ {
		for y := 0; y < self.height; y++ {
			col := colorful.Color{0.0, 0.0, 0.0}
			for xx := 0.0; xx < 1.0; xx += 0.1 {
				for yy := 0.0; yy < 1.0; yy += 0.1 {
					here := point{float64(x) + xx, float64(y) + yy}

					// Find the closest point
					cp := -1       // Index of closest point
					cv := 500000.0 // Distance from closest point
					for i, p := range self.points {
						dist := p.Dist(here)
						if dist < cv {
							cp = i
							cv = dist
						}
					}

					col.R += self.colors[cp].R / 100.0
					col.G += self.colors[cp].G / 100.0
					col.B += self.colors[cp].B / 100.0
				}
			}
			self.RGBA.Set(x, y, col.Clamped())
		}
	}

	for i, p := range self.points {
		self.points[i].X += p.Vx / self.fps
		self.points[i].Y += p.Vy / self.fps

		if self.points[i].X < 0 || self.points[i].X > float64(self.width) {
			self.points[i].Vx *= -1
			self.points[i].X = math.Max(math.Min(p.X, float64(self.width)), 0.0)
		}
		if self.points[i].Y < 0 || self.points[i].Y > float64(self.height) {
			self.points[i].Vy *= -1
			self.points[i].Y = math.Max(math.Min(p.Y, float64(self.height)), 0.0)
		}
	}

}
func (self *Voronoi) Close(l *log.Logger) {}

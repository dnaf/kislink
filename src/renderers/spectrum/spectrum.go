/*

   kislink
   Copyright (C) 2018  Katie Wolfe

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package spectrum

import (
	"github.com/lucasb-eyer/go-colorful"
	"gitlab.com/dnaf/go-cava"
	"image"
	"image/color"
	"log"
	"math"
)

type Spectrum struct {
	*image.RGBA

	width, height int

	bars []float64

	cava *cava.Cava
}

type SpectrumOpts struct {
	Width, Height int
}

func (self *Spectrum) Init(l *log.Logger, optsi interface{}) {
	opts := optsi.(*SpectrumOpts)
	self.width = opts.Width
	self.height = opts.Height
	self.RGBA = image.NewRGBA(image.Rect(0, 0, self.width, self.height))

	cfg := cava.DefaultConfig
	cfg.Bars = self.width
	cfg.Channels = cava.MONO

	// Spawn cava
	l.Println("Spawning cava")
	cavaInstance, err := cava.NewCava(cfg)
	if err != nil {
		panic(err)
	}

	self.bars = make([]float64, self.width)

	self.cava = cavaInstance

	go func() {
		for {
			bars, err := self.cava.Read()
			if err != nil {
				panic(err)
			}
			self.bars = bars
		}
	}()
}

func (self *Spectrum) GetDefaultOptions() interface{} {
	return &SpectrumOpts{
		Width:  18,
		Height: 6,
	}
}

func (self *Spectrum) Buffer(l *log.Logger) {
	bars := self.bars

	// Split bars into three bands for RGB color
	var col colorful.Color
	{
		// Red, green, and blue values
		// Corresponding to bass, mid, and treble respectively
		colv := [3]float64{0, 0, 0}

		numbars := float64(len(bars))        // Total number of bars
		partsize := float64(len(bars)) / 3.0 // Number of bars in each band

		for b, v := range bars {
			band := (float64(b) / numbars) * 3.0
			colv[int(band)] += v / partsize
		}

		h, s, _ := colorful.Color{colv[0], colv[1], colv[2]}.Hsv()
		col = colorful.Hsv(h, math.Min(s/2.0+0.5, 1.0), 1.0)
	}

	// Render the bars
	for b, v := range bars {
		vh := v * float64(self.height)  // The height of this bar in pixels
		vt := float64(self.height) - vh // The top pixel of this bar

		for y := 0; y < self.height; y++ {
			alpha := 0.0

			if y > int(vt) { // This pixel is completely colored by the bar
				alpha = 1
			} else if y == int(vt) { // This pixel is on the edge of the bar
				// Anti-alias the edge
				alpha = 1.0 - math.Mod(vt, 1.0)
			}
			h, s, v := col.Hsv()
			rr, gg, bb := colorful.Hsv(h, s, v*alpha).Clamped().RGB255()
			self.RGBA.Set(b, y, color.RGBA{rr, gg, bb, uint8(alpha * 255.0)})
		}
	}
}

func (self *Spectrum) Close(l *log.Logger) {
	self.cava.Close()
}

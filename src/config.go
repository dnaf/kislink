/*

   kislink
   Copyright (C) 2018  Katie Wolfe

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package main

import (
	"github.com/mitchellh/mapstructure"
	"gitlab.com/dnaf/kislink/src/displays/ckb"
	"gitlab.com/dnaf/kislink/src/displays/launchpadmk2"
	"gitlab.com/dnaf/kislink/src/displays/nulldisplay"
	"gitlab.com/dnaf/kislink/src/displays/shojiled"
	"gitlab.com/dnaf/kislink/src/renderers/spectrum"
	"gitlab.com/dnaf/kislink/src/renderers/voronoi"
	"gopkg.in/yaml.v2"
	"log"
	"reflect"
	"strings"
)

type YamlConfig struct {
	Displays  map[string]map[string]interface{}
	Renderers map[string]map[string]interface{}
}

type Config struct {
	Displays  []DisplayInstance
	Renderers []RendererInstance
}

func DecodeConfigString(input string) Config {
	config := YamlConfig{}

	displayList := []Display{
		&nulldisplay.NullDisplay{},
		&launchpadmk2.LaunchpadMK2{},
		&shojiled.ShojiLED{},
		&ckb.CKB{},
	}
	rendererList := []Renderer{
		&voronoi.Voronoi{},
		&spectrum.Spectrum{},
	}

	err := yaml.UnmarshalStrict([]byte(input), &config)
	if err != nil {
		panic(err)
	}

	rendererInstances := make([]RendererInstance, 0, len(config.Renderers))
	displayInstances := make([]DisplayInstance, 0, len(config.Displays))

	// Iterate over renderer configurations
	for name, opts := range config.Renderers {
		// Get type of renderer
		t := opts["type"].(string)

		// Match renderer name to struct
		var thisRenderer Renderer = nil
		for _, s := range rendererList {
			if strings.EqualFold(t, reflect.TypeOf(s).Elem().Name()) {
				// Who woulda thought copying an interface could be so hard?
				thisRenderer = reflect.New(reflect.ValueOf(s).Elem().Type()).Interface().(Renderer)
				break
			}
		}
		if thisRenderer == nil {
			log.Fatalf("Unable to find renderer type \"%s\"", t)
		}
		rendererOpts := thisRenderer.GetDefaultOptions()
		mapstructure.Decode(opts, rendererOpts)
		rendererInstances = append(rendererInstances, RendererInstance{
			ID:       name,
			Renderer: thisRenderer,
			Options:  rendererOpts,
		})
	}

	// Iterate over display configurations
	for name, opts := range config.Displays {
		// Get type of display
		t := opts["type"].(string)

		composite := make([]RendererCompositeSettings, 0)

		if v, err := opts["composite"]; err != false {
			list := v.([]interface{})
			composite = make([]RendererCompositeSettings, len(list))
			for i, in := range list {
				m := in.(map[interface{}]interface{})

				// Workaround for go-yaml/yaml#368
				// https://github.com/go-yaml/yaml/issues/368
				if v, exists := m[true]; exists {
					m["y"] = v
				}

				// Get intermediate representation of composite settings
				dataInt := struct {
					Id   string
					X, Y int
				}{}
				mapstructure.Decode(m, &dataInt)

				var renderer *RendererInstance = nil
				// Find renderer with the same ID
				for _, ren := range rendererInstances {
					if ren.ID == dataInt.Id {
						renderer = &ren
						break
					}
				}

				if renderer == nil {
					log.Fatalf("No renderer was found with the ID \"%s\"", dataInt.Id)
				}

				composite[i] = RendererCompositeSettings{
					Renderer: renderer,
					X:        dataInt.X,
					Y:        dataInt.Y,
				}
			}
		}

		// Match display name to struct
		var thisDisplay Display = nil
		for _, s := range displayList {
			if strings.EqualFold(t, reflect.TypeOf(s).Elem().Name()) {
				// Who woulda thought copying an interface could be so hard?
				thisDisplay = reflect.New(reflect.ValueOf(s).Elem().Type()).Interface().(Display)
				break
			}
		}
		if thisDisplay == nil {
			log.Fatalf("Unable to find display type \"%s\"", t)
		}
		displayOpts := thisDisplay.GetDefaultOptions()
		mapstructure.Decode(opts, displayOpts)

		displayInstances = append(displayInstances, DisplayInstance{
			ID:        name,
			Display:   thisDisplay,
			Options:   displayOpts,
			Composite: composite,
		})
	}

	return Config{
		Displays:  displayInstances,
		Renderers: rendererInstances,
	}
}

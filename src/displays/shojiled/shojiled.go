/*

   kislink
   Copyright (C) 2018  Katie Wolfe

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package shojiled

import (
	"github.com/tarm/serial"
	"image"
	"image/color"
	"log"
)

type ShojiLED struct {
	data         []byte
	databuffered []byte
	width        uint
	height       uint
	port         *serial.Port
}

type ShojiLEDOpts struct {
	Width, Height uint
	Brightness    float64
	Port          string
}

func (self *ShojiLED) Bounds() image.Rectangle {
	return image.Rect(0, 0, int(self.width), int(self.height))
}
func (self *ShojiLED) ColorModel() color.Model {
	return color.RGBAModel
}

func (self *ShojiLED) getPixOffset(x, y uint) uint {
	return (y*self.width+x)*3 + 1
}

func (self *ShojiLED) Set(x, y int, col color.Color) {
	if x >= 0 && y >= 0 && uint(x) < self.width && uint(y) < self.height {
		offset := self.getPixOffset(uint(x), uint(y))
		r, g, b, _ := col.RGBA()
		self.data[offset] = byte(r >> 8)
		self.data[offset+1] = byte(g >> 8)
		self.data[offset+2] = byte(b >> 8)
	}
}
func (self *ShojiLED) At(x, y int) color.Color {
	offset := self.getPixOffset(uint(x), uint(y))
	return color.RGBA{
		self.data[offset],
		self.data[offset+1],
		self.data[offset+2],
		255,
	}
}

func (self *ShojiLED) Init(l *log.Logger, optsi interface{}) {
	opts := optsi.(*ShojiLEDOpts)

	self.width = opts.Width
	self.height = opts.Height
	self.data = make([]byte, self.width*self.height*3+1)
	self.data[0] = uint8(opts.Brightness * 255.0)
	self.databuffered = make([]byte, len(self.data))
	copy(self.databuffered, self.data)

	l.Println("Opening port")
	port, err := serial.OpenPort(&serial.Config{
		Name: opts.Port,
		Baud: 115200,
	})
	if err != nil {
		panic(err)
	}

	self.port = port

	go self.run(l)
}

func (self *ShojiLED) Buffer(l *log.Logger) {
	copy(self.databuffered, self.data)
}

func (self *ShojiLED) run(l *log.Logger) {
	for {
		okbuf := make([]byte, 1)
		okt, err := self.port.Read(okbuf)
		if err != nil {
			l.Panic(err)
		}
		if okbuf[:okt][0] != 0x40 {
			l.Printf("Unexpected byte 0x%X received over serial", okbuf[:okt][0])
		}

		self.port.Write(self.databuffered)
	}
}
func (self *ShojiLED) Close(l *log.Logger) {
	err := self.port.Close()
	if err != nil {
		l.Panic(err)
	}
}

func (self *ShojiLED) GetDefaultOptions() interface{} {
	return &ShojiLEDOpts{
		Width:      8,
		Height:     8,
		Brightness: 1.0,
		Port:       "/dev/ttyACM0",
	}
}

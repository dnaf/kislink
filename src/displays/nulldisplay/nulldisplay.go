/*

   kislink
   Copyright (C) 2018  Katie Wolfe

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package nulldisplay

import (
	"image"
	"log"
)

type NullDisplay struct {
	*image.RGBA

	width, height int
}

type NullDisplayOpts struct {
	Width, Height int
}

func (self *NullDisplay) Init(l *log.Logger, optsi interface{}) {
	opts := optsi.(*NullDisplayOpts)
	self.width = opts.Width
	self.height = opts.Height

	self.RGBA = image.NewRGBA(image.Rect(0, 0, self.width, self.height))

	l.Println("Hanging out in the void, doing nothing")
}

func (self *NullDisplay) GetDefaultOptions() interface{} {
	return &NullDisplayOpts{
		Width:  16,
		Height: 16,
	}
}

func (self *NullDisplay) Buffer(l *log.Logger) {}
func (self *NullDisplay) Close(l *log.Logger)  {}

/*

   kislink
   Copyright (C) 2018  Katie Wolfe

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package launchpadmk2

import (
	"github.com/rakyll/portmidi"
	"image"
	"log"
	"strings"
)

type LaunchpadMK2 struct {
	*image.RGBA

	out *portmidi.Stream
}

func (self *LaunchpadMK2) Init(l *log.Logger, opts interface{}) {
	self.RGBA = image.NewRGBA(image.Rect(0, 0, 9, 9))

	l.Println("Initializing Portmidi")
	if err := portmidi.Initialize(); err != nil {
		panic(err)
	}

	l.Println("Searching for Launchpad")
	deviceID := portmidi.DeviceID(-1)
	for i := 0; i < portmidi.CountDevices(); i++ {
		info := portmidi.Info(portmidi.DeviceID(i))
		if strings.Contains(info.Name, "Launchpad MK2") {
			deviceID = portmidi.DeviceID(i)
			break
		}
	}
	if deviceID == -1 {
		panic("No Launchpad MK2 was detected")
	}

	l.Println("Opening output stream")
	output, err := portmidi.NewOutputStream(deviceID, 1024, 0) // TODO Configurable buffer size and latency
	if err != nil {
		panic(err)
	}
	self.out = output
}
func (self *LaunchpadMK2) Close(l *log.Logger) {
	l.Println("Closing output stream")
	self.out.Close()
	l.Println("Terminating Portmidi")
	portmidi.Terminate()
}
func (self *LaunchpadMK2) Buffer(l *log.Logger) {

	for x := 0; x < 9; x++ {
		buf := make([]byte, 0, 81*4+8) // 81 pads with 4 bytes each
		buf = append(buf, []byte{0xF0, 0x00, 0x20, 0x29, 0x02, 0x18, 0x0B}...)
		for y := 0; y < 9; y++ {
			var led byte
			if y > 0 {
				led = byte(x + (9-y)*10 + 1)
			} else {
				led = byte(x + 0x68)
			}
			col := self.RGBA.RGBAAt(int(x), int(y))
			buf = append(buf, []byte{led, col.R / 4, col.G / 4, col.B / 4}...)
		}
		buf = append(buf, 0xF7)
		self.out.WriteSysExBytes(portmidi.Time(), buf)
	}
}

func (self *LaunchpadMK2) GetDefaultOptions() interface{} {
	return struct{}{}
}

package ckb

import (
	"errors"
	"fmt"
	"gitlab.com/dnaf/go-ckb"
	"strings"
)

type CKBKeymap struct {
	match  string
	keymap [][]string
}

var CKBKeymaps []CKBKeymap = []CKBKeymap{
	{"K70", k70_keymap},
	{"SCIMITAR", scimitar_keymap},
}

func getKeymap(info *ckb.DeviceInfo) (CKBKeymap, error) {
	for _, keymap := range CKBKeymaps {
		if strings.Contains(info.Description, keymap.match) {
			return keymap, nil
		}
	}
	return CKBKeymap{}, errors.New(fmt.Sprintf("Cannot find keymap for CKB device type %q", info.Description))
}

var k70_keymap = [][]string{
	[]string{"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "light", "lock", "", "", "", "mute", ""},
	[]string{"esc", "", "f1", "f2", "f3", "f4", "", "f5", "f6", "f7", "f8", "f9", "f10", "f11", "f12", "prtscn", "scroll", "pause", "stop", "prev", "play", "next"},
	[]string{"grave", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "minus", "equal", "bspace", "bspace", "ins", "home", "pgup", "numlock", "numslash", "numstar", "numminus"},
	[]string{"tab", "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "lbrace", "rbrace", "bslash", "bslash", "del", "end", "pgdn", "num7", "num8", "num9", "numplus"},
	[]string{"caps", "a", "s", "d", "f", "g", "h", "j", "k", "l", "colon", "quote", "enter", "enter", "enter", "", "", "", "num4", "num5", "num6", "numplus"},
	[]string{"lshift", "z", "x", "c", "v", "b", "n", "m", "comma", "dot", "slash", "rshift", "rshift", "rshift", "rshift", "", "up", "", "num1", "num2", "num3", "numenter"},
	[]string{"lctrl", "lwin", "lalt", "", "", "space", "", "", "", "ralt", "rwin", "rmenu", "rctrl", "rctrl", "rctrl", "left", "down", "right", "num0", "num0", "numdot", "numenter"},
}

var scimitar_keymap [][]string = [][]string{
	[]string{"", "", "back"},
	[]string{"dpi", "wheel", ""},
	[]string{"thumb", "", ""},
	[]string{"", "front", ""},
}

// vim: set nowrap

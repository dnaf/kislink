package ckb

import (
	"errors"
	"fmt"
	"gitlab.com/dnaf/go-ckb"
	"image"
	"image/color"
	"log"
	"strings"
)

type CKB struct {
	*image.RGBA

	device *ckb.Device
	keymap CKBKeymap
}

type CKBOpts struct {
	MatchModel string
	Mode       int
}

func chk(err error) {
	if err != nil {
		panic(err)
	}
}

// matchDevice finds the first CKB device with the given string in its model name and returns its ckb.DeviceInfo.
// It will return an error if no matching devices are found.
func matchDevice(match string) (*ckb.DeviceInfo, error) {
	// Get list of devices
	devices, err := ckb.ListDevices()
	if err != nil {
		return nil, err
	}

	// And find the right one
	for _, dev := range devices {
		if strings.Contains(dev.Description, match) {
			return &dev, nil
		}
	}

	return nil, errors.New(fmt.Sprintf("Unable to find device matching %q", match))
}

func (self *CKB) Init(l *log.Logger, optsi interface{}) {
	opts := optsi.(*CKBOpts)

	// Find the right device
	deviceInfo, err := matchDevice(opts.MatchModel)
	chk(err)

	// See if we have a keymap for it
	self.keymap, err = getKeymap(deviceInfo)
	chk(err)

	// Open it
	self.device, err = ckb.OpenDevice(deviceInfo.Path)
	chk(err)

	// Create our image
	self.RGBA = image.NewRGBA(image.Rect(0, 0, len(self.keymap.keymap[0]), len(self.keymap.keymap)))

	// Set the device mode
	self.device.SetMode(opts.Mode)
	self.device.Switch()

	// Make it all black
	self.device.SetAll(color.RGBA{0, 0, 0, 255})
}
func (self *CKB) GetDefaultOptions() interface{} {
	return &CKBOpts{
		MatchModel: "",
		Mode:       1,
	}
}
func (self *CKB) Close(l *log.Logger) {
	defer self.device.Close()
}

func (self *CKB) Buffer(logger *log.Logger) {
	b := self.Bounds()
	for x := b.Min.X; x < b.Max.X; x++ {
		for y := b.Min.Y; y < b.Max.Y; y++ {
			v := self.At(x, y)
			self.device.Set(self.keymap.keymap[y][x], v)
		}
	}
}

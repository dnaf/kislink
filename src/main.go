/*

   kislink
   Copyright (C) 2018  Katie Wolfe

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package main

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"reflect"
	"sync"
	"syscall"
	"time"
)

var Version string = "v?.?.?-unknown"

var displays []DisplayInstance
var renderers []RendererInstance

func init() {
	initFlags()
}

type RendererInstance struct {
	ID       string
	Renderer Renderer
	Options  interface{}
	Logger   *log.Logger
}
type RendererCompositeSettings struct {
	Renderer *RendererInstance
	X, Y     int
}
type DisplayInstance struct {
	ID        string
	Display   Display
	Options   interface{}
	Composite []RendererCompositeSettings
	Logger    *log.Logger
}

func main() {
	fmt.Printf("kislink %s\n", Version)

	parseFlags()

	if showVersionAndExit {
		return
	}

	// Capture interrupts
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	// Use default config path if none was given
	if configPath == "" {
		var err error
		configPath, err = GetConfigPath()
		if err != nil {
			panic(err)
		}
	}

	// Read & parse config
	dat, err := ioutil.ReadFile(configPath)
	if err != nil {
		panic(err)
	}
	config := DecodeConfigString(string(dat))

	renderers = config.Renderers
	displays = config.Displays

	// Initialize renderers
	for i := range renderers {
		// Create logger
		prefix := reflect.TypeOf(renderers[i].Renderer).Elem().Name() + ":" + renderers[i].ID + " \t"
		renderers[i].Logger = log.New(os.Stdout, prefix, log.LstdFlags)

		// Initialize renderer
		renderers[i].Renderer.Init(renderers[i].Logger, renderers[i].Options)
		defer renderers[i].Renderer.Close(renderers[i].Logger)
	}

	// Initialize devices
	for i := range displays {
		// Create logger
		prefix := reflect.TypeOf(displays[i].Display).Elem().Name() + ":" + displays[i].ID + " \t"
		displays[i].Logger = log.New(os.Stdout, prefix, log.LstdFlags)

		// Initialize display
		displays[i].Display.Init(displays[i].Logger, displays[i].Options)
		defer displays[i].Display.Close(displays[i].Logger)
	}

	// Initialize GUI
	guiStopChannel := make(chan struct{})
	if guiEnabled {
		initGUI(guiStopChannel)
	}

MainLoop:
	for {
		var rendererWg, deviceWg sync.WaitGroup

		// Buffer all renderers
		for _, ren := range renderers {
			rendererWg.Add(1)
			ren := ren // Capture variable

			go func(ren RendererInstance) {
				defer rendererWg.Done()
				ren.Renderer.Buffer(ren.Logger)
			}(ren)
		}

		// Wait for all renderers to finish buffering
		rendererWg.Wait()

		for _, dev := range displays {
			deviceWg.Add(1)
			dev := dev // Capture variable

			go func() {
				defer deviceWg.Done()

				// Clear the buffer
				draw.Draw(dev.Display, dev.Display.Bounds(), &image.Uniform{color.RGBA{0, 0, 0, 255}}, image.ZP, draw.Src)

				// Composite the renderers
				for _, ren := range dev.Composite {
					rect := image.Rect(ren.X, ren.Y, ren.X+ren.Renderer.Renderer.Bounds().Dx(), ren.Y+ren.Renderer.Renderer.Bounds().Dy())
					draw.Draw(dev.Display, rect, ren.Renderer.Renderer, image.ZP, draw.Over)
				}

				// End buffer
				dev.Display.Buffer(dev.Logger)
			}()

		}

		// Wait for all devices to finish
		deviceWg.Wait()

		// Update GUI
		if guiEnabled {
			go updateGUI()
		}

		// Gracefully exit
		select {
		case <-sigs:
			log.Println("Received interrupt; breaking loop")
			break MainLoop
		default:
		}

		// Sleep a lil
		time.Sleep(5 * time.Millisecond)
	}

	if guiEnabled {
		guiStopChannel <- struct{}{}
	}
}

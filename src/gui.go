/*

   kislink
   Copyright (C) 2018  Katie Wolfe

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package main

import (
	"github.com/fogleman/gg"
	"golang.org/x/exp/shiny/driver"
	"golang.org/x/exp/shiny/screen"
	"image"
	"math"
)

var guiCtx *gg.Context
var guiWindow screen.Window
var guiBuffer screen.Buffer
var guiLocked bool

func initGUI(stop chan struct{}) {
	ready := make(chan struct{})
	go driver.Main(func(s screen.Screen) {
		// Create window
		w, err := s.NewWindow(nil)
		if err != nil {
			panic(err)
		}
		defer w.Release()

		// Create buffer
		b, err := s.NewBuffer(image.Point{800, 600})
		if err != nil {
			panic(err)
		}
		defer b.Release()

		// Create rendering context
		guiCtx = gg.NewContextForRGBA(b.RGBA())

		guiWindow = w
		guiBuffer = b

		ready <- struct{}{}
		<-stop
	})

	<-ready
}

func drawPreview(ctx *gg.Context, img image.Image, width float64, x float64, y float64) float64 {
	h := 4.0
	bounds := img.Bounds()
	dx := bounds.Dx()
	dy := bounds.Dy()

	if float64(dx) > width { // Too wide
		ctx.DrawStringAnchored("too wide", x+width/2.0, y, 0.5, 1)
		h += ctx.FontHeight()
	} else {
		for s := 48; s > 0; s-- {
			if float64(dx*s) <= width {
				for xx := 0; xx < dx; xx++ {
					for yy := 0; yy < dy; yy++ {
						boxw := float64(s)
						boxw = math.Max(boxw-1.0, 1.0)
						ctx.SetColor(img.At(xx, yy))
						ctx.DrawRectangle(x+float64(xx*s), h+y+float64(yy*s), boxw, boxw)
						ctx.Fill()
					}
				}
				h += float64(dy * s)
				break
			}
		}
	}
	return h
}

func updateGUI() {
	if guiLocked {
		return
	}
	guiLocked = true
	defer func() { guiLocked = false }()
	ctx := guiCtx
	ctx.SetRGB(0.1, 0.1, 0.1)
	ctx.Clear()

	renJoints := make(map[string]float64)

	colWidth := 200.0
	graphWidth := 400.0

	fheight := ctx.FontHeight()

	// Draw renderers
	{
		reny := 0.0
		for _, ren := range renderers {
			h := 0.0 // the height of this box

			ctx.SetRGB(1, 1, 1)

			// Draw title
			ctx.DrawStringAnchored("["+ren.ID+"]", colWidth/2.0, reny, 0.5, 1)
			h += fheight

			// Draw preview
			h += drawPreview(ctx, ren.Renderer, colWidth, 0, reny+h)

			// Create joint
			renJoints[ren.ID] = reny + h/2.0

			// Increment height
			reny += h + 10
		}
	}

	// Draw displays
	{
		disy := 0.0
		colx := colWidth + graphWidth
		for _, dis := range displays {
			h := 0.0 // the height of this box

			ctx.SetRGB(1, 1, 1)

			// Draw title
			ctx.DrawStringAnchored("["+dis.ID+"]", colx+colWidth/2.0, disy, 0.5, 1)
			h += fheight

			// Draw preview
			h += drawPreview(ctx, dis.Display, colWidth, colx, disy+h)

			// Draw joints
			ctx.SetRGB(1, 1, 1)
			ctx.SetLineWidth(2.5)
			for _, comp := range dis.Composite {
				jointy := renJoints[comp.Renderer.ID]
				ctx.MoveTo(colWidth, jointy)
				ctx.QuadraticTo(colWidth+graphWidth/2.0, disy+h/2.0, colWidth+graphWidth, disy+h/2.0)
				ctx.Stroke()
			}

			disy += h + 10
		}
	}

	guiWindow.Upload(image.Point{}, guiBuffer, guiBuffer.Bounds())
}

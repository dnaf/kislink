package main

import (
	"os"
	"os/user"
	"path"
)

// GetConfigPath returns a path to a readable config file.
// GetConfigPath will return NoConfigFound if no config was found.
func GetConfigPath() (string, error) {
	configHome := os.Getenv("XDG_CONFIG_HOME")
	if configHome == "" {
		u, err := user.Current()
		if err != nil {
			return "", err
		}
		if u.HomeDir == "" {
			return "", NoConfigFound
		}
		configHome = path.Join(u.HomeDir, ".config")
	}

	ourConfigDir := path.Join(configHome, "kislink")
	return testDirectoryForConfig(ourConfigDir)
}

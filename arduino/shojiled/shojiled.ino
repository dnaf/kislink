#include "FastLED.h"

#define DATA_PIN 8
#define LED_TYPE WS2812
#define COLOR_ORDER GRB
#define NUM_REAL_LEDS 300
#define NUM_LEDS (6 * 6 * 3)
#define BUF_SIZE (NUM_LEDS * 3 + 1)

CRGB leds[NUM_REAL_LEDS] = {0};

const int led_map[NUM_LEDS] = {
  39,37,35,33,31,29,24,22,20,18,16,14,11,9,7,5,3,1,
  48,50,52,54,56,58,61,63,65,67,69,71,74,76,78,80,82,84,
  131,129,127,125,123,121,116,114,112,110,108,106,103,101,99,97,95,93,
  189,187,185,183,181,179,174,172,170,168,166,164,161,159,157,155,153,151,
  198,200,202,204,206,208,211,213,215,217,219,221,224,226,228,230,232,234,
  281,279,277,275,273,271,266,264,262,260,258,256,253,251,249,247,245,243
};

void setup() {
  FastLED.addLeds<LED_TYPE, DATA_PIN, COLOR_ORDER>(leds, NUM_REAL_LEDS).setCorrection(0xFFB0F0);
	Serial.begin(115200);
	Serial.setTimeout(1000);
}

void loop() {
  Serial.write(0x40);
  uint8_t cmd_buffer[BUF_SIZE];
  int bytesread = Serial.readBytes(cmd_buffer, BUF_SIZE);
  if (bytesread == BUF_SIZE) {
    FastLED.setBrightness(cmd_buffer[0]);

    for (uint8_t i = 0; i < NUM_LEDS; i++) {
      uint16_t p = i * 3 + 1; // The index of the red component of this pixel in the buffer
      leds[led_map[i]] = CRGB(cmd_buffer[p], cmd_buffer[p + 1], cmd_buffer[p + 2]);
    }
  }
  FastLED.show();
}

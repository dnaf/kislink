GO=go
GOBUILD=$(GO) build
GOGET=$(GO) get
GOTEST=$(GO) test

LDFLAGS=-ldflags "-X main.Version=$(VERSION)"
TESTFLAGS=-v -coverprofile=cover.out -covermode=atomic -race

BUILD=$(GOBUILD) $(LDFLAGS)
TEST=$(GOTEST) $(LDFLAGS) $(TESTFLAGS)

PLATFORMS=linux-amd64

PLATFORMDIRS=$(foreach plat,$(PLATFORMS),release/$(plat))

VERSION:=$(shell git describe --tags --dirty)
RELEASE=kislink_$(VERSION)_$(platform)

platform=$(@:release/%=%)# linux-amd64
platsp=$(subst -, ,$(platform))# linux amd64
os=$(word 1, $(platsp))# linux
arch=$(word 2, $(platsp))# amd64


kislink: src
	$(BUILD) -o $@ ./src

test: src
	$(TEST) ./src

.PHONY: deps
deps:
	$(GOGET) -v -d ./src

.PHONY: testdeps
testdeps:
	$(GOGET) -v -t -d ./src

.PHONY: $(PLATFORMDIRS)
$(PLATFORMDIRS):
	mkdir -p release/$(RELEASE)
	GOOS=$(os) GOARCH=$(arch) $(BUILD) -o release/$(RELEASE)/kislink ./src
	cp LICENSE config.yaml.example release/$(RELEASE)/
	tar -czvf release/$(RELEASE).tar.gz release/$(RELEASE)

.PHONY: release
release: $(PLATFORMDIRS)
